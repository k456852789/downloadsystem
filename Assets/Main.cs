﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    public IEnumerator Start()
    {
        Debug.Log("Start");

        //yield return Test();
        //yield return UpdateAllGroupsCoro();
        //yield return Download_1();
        //yield return Download_2();
        yield return Download_3();
        //yield return Download_4();

        yield return Load();
    }

    private IEnumerator Load()
    {
        var canvas = GameObject.Find("Canvas");
        var local = Addressables.LoadAssetAsync<GameObject>("Local");
        yield return local;

        var localButton = Instantiate(local.Result);
        localButton.transform.SetParent(canvas.transform);
        localButton.transform.localPosition = new Vector3(0, 0, 0);

        var remote = Addressables.LoadAssetAsync<GameObject>("Remote");
        yield return remote;

        var remoteButton = Instantiate(remote.Result);
        remoteButton.transform.SetParent(canvas.transform);
        remoteButton.transform.localPosition = new Vector3(0, 150, 0);
    }

    private IEnumerator Download_1()
    {
        AsyncOperationHandle<IResourceLocator> init = Addressables.InitializeAsync();
        yield return init;

        //Clear all cached AssetBundles
        AsyncOperationHandle<bool> clear = Addressables.ClearDependencyCacheAsync(Addressables.ResourceLocators, true);
        yield return clear;

        //Check the download size
        AsyncOperationHandle<long> getDownloadSize = Addressables.GetDownloadSizeAsync(Addressables.ResourceLocators);
        yield return getDownloadSize;

        Debug.Log($"Download Size : { getDownloadSize.Result }");

        //If the download size is greater than 0, download all the dependencies.
        if (getDownloadSize.Result > 0)
        {
            AsyncOperationHandle downloadDependencies = Addressables.DownloadDependenciesAsync(Addressables.ResourceLocators);
            yield return downloadDependencies;
        }
    }


    private IEnumerator Download_2()
    {
        AsyncOperationHandle<IResourceLocator> init = Addressables.InitializeAsync();
        yield return init;

        //Clear all cached AssetBundles
        AsyncOperationHandle<bool> clear = Addressables.ClearDependencyCacheAsync(Addressables.ResourceLocators, true);
        yield return clear;

        AsyncOperationHandle<long> getDownloadSize = Addressables.GetDownloadSizeAsync(Addressables.ResourceLocators);
        yield return getDownloadSize;
        Debug.Log($"Downlad Size : {getDownloadSize.Result}");

        AsyncOperationHandle downloadDependencies = Addressables.DownloadDependenciesAsync(Addressables.ResourceLocators);
        yield return downloadDependencies;
    }

    private IEnumerator Download_3()
    {
        // 只要打包的時候不要將Disable Catalog Update on Startup勾選上就行，這樣初始化的時候會自動更新Catalog到最新
        yield return Addressables.InitializeAsync();

        IEnumerable<IResourceLocator> locators = Addressables.ResourceLocators;
        List<object> keys = new List<object>();

        // 暴力遍歷所有的key
        foreach (var locator in locators)
        {
            foreach (var key in locator.Keys)
            {
                keys.Add(key);
            }
        }

        // Clear all cached AssetBundles
        //Addressables.ClearDependencyCacheAsync(keys as IEnumerable);

        var handle = Addressables.GetDownloadSizeAsync(keys as IEnumerable);
        yield return handle;
        long downloadSize = handle.Result;

        Debug.Log($"Download Size : {downloadSize}");

        if (downloadSize > 0)
        {
            yield return Addressables.DownloadDependenciesAsync(keys as IEnumerable, Addressables.MergeMode.Union, true);
        }
    }
}